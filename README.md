# MedUX Online
[![Documentation Status](https://readthedocs.org/projects/medux-online/badge/?version=latest)](https://medux-online.readthedocs.io/en/latest/?badge=latest)

Online companion for [MedUX](https://medux.at) and standalone patients medication request helper as well as some other tools around the MedUX ecosystem.

# Installation

You can see more in the [MedUX Online documentation](https://medux-online.readthedocs.io)
