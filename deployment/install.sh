#!/bin/sh

DJANGO_USER=medux_online

die() {
  echo $1
  exit 1
}



if [ "${DOMAIN}" = "" ]; then
  die "Please set DOMAIN variable to your FQDN."
fi

sudo useradd --create-home --system --user-group --shell /bin/bash ${DJANGO_USER} >/dev/null 2>&1

while [ ${HOMEPAGE_NAME} = "" ]; do
  read -p "Homepage name: " HOMEPAGE_NAME
done

while [ ${HOMEPAGE_LOGO_URL} = "" ]; do
  read -p "Homepage logo URL: " HOMEPAGE_LOGO_URL
done

while [ ${HOMEPAGE_MAIN_URL} = "" ]; do
  read -p "Homepage main URL: " HOMEPAGE_MAIN_URL
done

sudo apt-get install -y python3-dev default-libmysqlclient-dev build-essential \
      python3 python3-pip python3-virtualenv nginx uwsgi

#TODO: as normal user
pip install -r requirements.txt
pip install mysqlclient gettext

cat <<EOF >.env
SECRET_KEY="$(python3 -c 'import secrets; print(secrets.token_urlsafe())')"
DEBUG=False
ALLOWED_HOSTS=localhost,127.0.0.1,${DOMAIN}
HOMEPAGE_NAME="${HOMEPAGE_NAME}"
HOMEPAGE_LOGO_URL=${HOMEPAGE_LOGO_URL}
HOMEPAGE_MAIN_URL=${HOMEPAGE_MAIN_URL}
DB_ENGINE=django.db.backends.mysql
DB_NAME=medux_online
DB_PASSWORD="$(python3 -c 'import secrets; print(secrets.token_urlsafe())')"
EOF

sudo mv gunicorn.socket /etc/systemd/system/
sudo mv gunicorn.service /etc/systemd/system/
sudo mv medux_online.conf /etc/nginx/sites-available/
sudo ln -s /etc/nginx/sites-available/medux_online.conf /etc/nginx/sites-enabled/
sudo systemctl start --enable gunicorn
sudo systemctl start --enable nginx
