from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Row, Column, Submit
from django import forms
from django.core.validators import RegexValidator
from django.utils.translation import gettext_lazy as _
from polymorphic.contrib.extra_views import PolymorphicInlineFormSet
from polymorphic.formsets import PolymorphicFormSetChild

from .models import (
    Block,
    Header,
    News,
    OpeningHours,
    Gallery,
    Team,
    Contact,
    Map,
    Footer,
    Homepage,
)

DOMAIN_REGEX = "^((?!-))(xn--)?[a-z0-9][a-z0-9-_]{0,61}[a-z0-9]{0,1}\.(xn--)?([a-z0-9\-]{1,61}|[a-z0-9-]{1,30}\.[a-z]{2,})$"


# def domain_already_taken_validator(domain) -> bool:
#     """returns True if no other participant has this domain registered."""
#     return not Homepage.objects.filter(domain=domain).exists()


class HomepageForm(forms.ModelForm):
    class Meta:
        model = Homepage
        fields = ["name", "domain", "subtitle", "theme", "logo"]
        localized_fields = "__all__"  # FIXME: fields don't get localized.

    name = forms.CharField(
        strip=True,
        help_text=_(
            "The title of the homepage which gets displayed on the front page."
        ),
    )
    domain = forms.CharField(
        strip=True,
        help_text=_("The domain that is registered for the homepage."),
        validators=[
            RegexValidator(
                regex=DOMAIN_REGEX, message=_("Please enter a valid domain name.")
            ),
            # domain_already_taken_validator,
        ],
    )
    subtitle = forms.CharField(
        strip=True,
        required=False,
    )

    def __init__(self, **kwargs):
        super().__init__(**kwargs)

        self.helper = FormHelper()
        self.helper.layout = Layout(
            Row(Column("name"), Column("subtitle")),
            Row(Column("domain"), Column("theme"), Column("logo")),
        )
        self.helper.add_input(Submit("submit", _("Save")))

        if self.instance.pk:
            self.fields["domain"].disabled = True


class BlockSortForm(forms.Form):
    pass


class BlocksInline(PolymorphicInlineFormSet):
    model = Block
    formset_children = [
        PolymorphicFormSetChild(Header),
        PolymorphicFormSetChild(News),
        PolymorphicFormSetChild(Map),
        PolymorphicFormSetChild(OpeningHours),
        PolymorphicFormSetChild(Contact),
        PolymorphicFormSetChild(Team),
        PolymorphicFormSetChild(Gallery),
        PolymorphicFormSetChild(Footer),
    ]
    exclude = ["tenant", "weight", "homepage", "visible"]


class BlockForm(forms.ModelForm):
    """A Basic form that represents a `Block` model

    You can subclass this Form and make more specialized Forms. Just add a `Meta` class
    and specify the `model` there, and the excluded fields, mostly they will be
    `exclude = ["tenant", "content"]`. You don't have to subclass BlockForm.Meta explicitly then.
    """

    class Meta:
        model = Block
        exclude = ["tenant", "weight", "homepage", "visible"]

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_tag = False
        # self.helper.form_action = (
        #     reverse_lazy("block-update", kwargs={"pk": self.instance.pk})
        #     if self.instance
        #     else "."
        # )
        # self.helper.add_input(
        #     Submit("submit", _("Save"), css_class="btn btn-sm btn-primary")
        # )
        # pk = self.instance.pk if self.instance else "new"
        # self.helper.attrs = {
        #     "id": f"edit-block-{pk}-form",
        # }


class HeaderForm(BlockForm):
    class Meta(BlockForm.Meta):
        model = Header


class NewsForm(BlockForm):
    class Meta(BlockForm.Meta):
        model = News


class OpeningHoursForm(BlockForm):
    class Meta(BlockForm.Meta):
        model = OpeningHours


class MapForm(BlockForm):
    class Meta(BlockForm.Meta):
        model = Map


class ContactForm(BlockForm):
    class Meta(BlockForm.Meta):
        model = Contact


class TeamForm(BlockForm):
    class Meta(BlockForm.Meta):
        model = Team


class GalleryForm(BlockForm):
    class Meta(BlockForm.Meta):
        model = Gallery


class FooterForm(BlockForm):
    class Meta(BlockForm.Meta):
        model = Footer


all_forms = {
    "block": BlockForm,
    "header": HeaderForm,
    "map": MapForm,
    "opening_hours": OpeningHoursForm,
    "team": TeamForm,
    "news": NewsForm,
    "gallery": GalleryForm,
    "contact": ContactForm,
    "footer": FooterForm,
}
# all_inlines = {
#     "block": BlockInline,
#     "header": HeaderInline,
#     "map": MapInline,
#     "opening_hours": OpeningHoursInline,
#     "team": TeamInline,
#     "news": NewsInline,
#     "gallery": GalleryInline,
#     "contact": ContactInline,
#     "footer": FooterInline,
# }


class HomepageDetailForm(forms.ModelForm):
    # FIXME HomepageDetailForm does not work properly
    class Meta:
        model = Homepage
        fields = "__all__"
