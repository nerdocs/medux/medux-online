from django.urls import path, include

from . import views
from ...core.api.interfaces import IDashboardURL

# from ...core.views import TermsView, PrivacyView

app_name = "homepage"

homepage_urlpatterns = [
    path("", views.HomepageList.as_view(), name="list"),
    path("add/", views.HomepageCreate.as_view(), name="add"),
    path("<int:pk>/", views.HomepageUpdate.as_view(), name="detail"),
    path(
        "<int:pk>/update/",
        views.HomepageUpdate.as_view(),
        name="update",
    ),
    path(
        "<int:pk>/delete/",
        views.HomepageDelete.as_view(),
        name="delete",
    ),
    path("<int:pk>/blocks/", views.BlockList.as_view(), name="block-list"),
    path(
        "<int:pk>/blocks/sort/",
        views.BlockSort.as_view(),
        name="block-sort",
    ),
    path(
        "<int:homepage_pk>/blocks/add/<str:block_type>/",
        views.BlockCreate.as_view(),
        name="block-add",
    ),
]

block_urlpatterns = [
    path("<int:pk>/", views.BlockDetail.as_view(), name="detail"),
    path("<int:pk>/update/", views.BlockUpdate.as_view(), name="update"),
    path("<int:pk>/delete/", views.BlockDelete.as_view(), name="delete"),
]


class HomepageDashboardURLs(IDashboardURL):
    urlpatterns = [
        path(
            "homepage/",
            include((homepage_urlpatterns, "homepage"), namespace="homepage"),
        ),
        path("block/", include((block_urlpatterns, "block"), namespace="block")),
    ]


root_urlpatterns = [
    path("", views.HomepageView.as_view(), name="home"),
    # path("terms/", TermsView.as_view(), name="terms"),
    # path("privacy/", PrivacyView.as_view(), name="privacy"),
]
