from medux_online.core.tests import MeduxOnlineTestCase
from medux_online.plugins.homepage.models import Homepage


class HomepageTestCase(MeduxOnlineTestCase):
    """A TestCase for usage with all homepage tests.

    It creates a basic homepage environment with a
    Homepage named ``homepage.example.com``.
    """

    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.site = Homepage.objects.create(
            domain="homepage.example.com", tenant_id=1, theme_id=1
        )
        cls.site.save()
        cls.server_name = "homepage.example.com"
