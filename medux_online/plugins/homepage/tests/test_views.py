from django.conf import settings
from django.contrib.auth.models import Group
from django.urls import reverse

from medux_online.plugins.homepage.models import Homepage
from medux_online.plugins.homepage.tests import HomepageTestCase


class HomepageViewsTest(HomepageTestCase):
    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        site_editors = Group.objects.get(name="Site editors")
        cls.user1.groups.add(site_editors)
        cls.site1 = Homepage.objects.create(
            name="Example 123", domain="example123.com", tenant=cls.tenant1
        )

    def test_homepage_permission(self) -> None:
        self.client.force_login(self.user1)
        response = self.client.get("/")
        self.assertEqual(response.status_code, 200)

    def test_dashboard_not_available(self) -> None:
        settings.SITE_ID = self.site1.id
        self.client.force_login(self.user1)
        response = self.client.get(reverse("index"))
        self.assertEqual(403, response.status_code)

    # TODO Admin site must not be available on HomepageSites
    # def test_admin_not_available(self) -> None:
    #     response = self.client.get("/admin/")
    #     self.assertEqual(
    #         response.status_code,
    #         404,
    #         "Admin site must not be available on HomepageSites",
    #     )
