import logging

from django.contrib import messages
from django.contrib.auth.mixins import PermissionRequiredMixin
from django.db.models import Manager
from django.http import HttpResponseRedirect
from django.shortcuts import redirect
from django.template.loader import get_template
from django.urls import reverse_lazy, reverse
from django.utils.translation import gettext_lazy as _
from django.views.generic import (
    TemplateView,
    DetailView,
    CreateView,
    ListView,
    UpdateView,
    DeleteView,
)

from medux.common.api.http import HttpResponseEmpty
from medux.common.api.interfaces import ModalFormViewMixin
from medux.common.htmx.mixins import HtmxResponseMixin
from medux.common.mixins import TenantPermissionRequiredMixin
from medux.common.views import DashboardMixin, AutoFocusMixin
from .forms import (
    all_forms,
    BlockForm,
    HomepageForm,
)
from .models import Block, Homepage

logger = logging.getLogger(__file__)

########################### Homepage ###########################


class HomepageView(TemplateView):
    """
    This is a wrapper view, which combines some of other views into one.

    It shows the main homepage to the world.
    """

    template_name = "homepage/homepage.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        homepage = self.request.homepage
        if not homepage:
            homepage = self.request.session.get("homepage")
        if isinstance(homepage, int):
            homepage = Homepage.objects.get(pk=homepage)
        blocklist = []
        weights = []
        if homepage:
            blocks = homepage.blocks.filter(visible=True).order_by("weight")
            theme = homepage.theme

            for block in blocks:

                # render each block as an own view...
                template = get_template(f"themes/{theme.slug}/{block.template_name}")
                # fetch standard fields...
                context = {
                    "title": block.title,
                    "content": block.content,
                    "weight": block.weight,
                    "show_title": block.show_title,
                }
                # ... and additional fields for polymorphic child models
                for field_name in block.additional_fields:
                    field = getattr(block, field_name)
                    # if field is a FK, M2M etc., follow that link...
                    if isinstance(field, Manager):
                        field = field.all()
                    context[field_name] = field
                view = template.render(context, self.request)

                # ... and add it to the context at the correct position
                index = len(weights)
                for i in range(len(weights)):
                    if weights[i] > block.weight:
                        index = i
                        break

                blocklist.insert(index, view)
                weights.insert(index, block.weight)

            context["blocks"] = blocklist
        return context

    def get(self, request, *args, **kwargs):
        context = self.get_context_data(**kwargs)
        # tenant = request.user.tenant
        # if tenant:
        #     context["header"] = Header.objects.filter(tenant=tenant).first()
        #     context["homepage"] = Homepage.objects.filter(tenant=tenant).first()
        if not self.request.homepage:
            # TODO show "under construction" site
            return redirect(reverse("login"))
        return super().get(request, context=context, *args, **kwargs)


class HomepageList(PermissionRequiredMixin, DashboardMixin, ListView):
    permission_required = "homepage.view_homepage"
    template_name = "homepage/homepage_list.html"

    def get_queryset(self):
        return Homepage.objects.filter(tenant=self.request.user.tenant)


class HomepageCreate(
    PermissionRequiredMixin, AutoFocusMixin, DashboardMixin, CreateView
):
    model = Homepage
    form_class = HomepageForm
    autofocus_field_name = "name"
    permission_required = "homepage.add_homepage"
    extra_context = {"post_url": "homepage:add", "post_attr": ""}

    def form_valid(self, form):
        homepage = form.save(commit=False)
        homepage.tenant = self.request.user.tenant
        homepage.save()
        return redirect(reverse("homepage:list"))


# class HomepageDetail(PermissionRequiredMixin, DashboardMixin, DetailView):
#     template_name = "homepage/homepage_form.html"
#     model = Homepage
#     permission_required = "homepage.view_homepage"


class HomepageUpdate(
    PermissionRequiredMixin,
    AutoFocusMixin,
    DashboardMixin,
    UpdateView,
):
    model = Homepage
    form_class = HomepageForm
    autofocus_field_name = "name"
    permission_required = "homepage.change_homepage"
    inlines = []
    success_url = reverse_lazy("homepage:list")


class HomepageDelete(PermissionRequiredMixin, DeleteView):
    model = Homepage
    permission_required = "homepage.delete_homepage"

    def form_valid(self, form):
        if not self.object.tenant == self.request.user.tenant:
            raise PermissionError(
                _("You can't delete another tenant's %s.").format(
                    self.model._meta.verbose_name
                )
            )
        self.object.delete()
        return redirect(reverse("homepage:list"))


########################### Block ###########################


class BlockList(PermissionRequiredMixin, ListView):
    permission_required = "homepage.change_homepage"
    modal_title = _("Add block")
    success_event = "medux:block:added"

    def get_queryset(self):
        """Get all blocks that belong to given homepage, if user's tenant matches."""
        return Block.objects.filter(
            homepage_id=self.kwargs.get("pk"), homepage__tenant=self.request.user.tenant
        ).order_by("weight")

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super().get_context_data(object_list=object_list, **kwargs)
        available_blocks = []
        for slug, form in all_forms.items():
            # TODO: some blocks may appear more than once
            if not slug in [block.btype() for block in self.object_list] or slug in [
                "block"
            ]:
                available_blocks.append(
                    {
                        "slug": slug,
                        "title": str(form._meta.model._meta.verbose_name),
                    }
                )
        context.update(
            {
                "homepage": Homepage.objects.get(
                    pk=self.kwargs.get("pk"), tenant=self.request.user.tenant
                ),
                "available_blocks": available_blocks,
            }
        )
        return context


class BlockSort(PermissionRequiredMixin, ListView):
    permission_required = "homepage.change_homepage"
    template_name = "homepage/block_list_sort.html"
    model = Block

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update(
            {
                "homepage": Homepage.objects.get(
                    pk=self.kwargs.get("pk"),
                    tenant=self.request.user.tenant,
                )
            }
        )
        return context

    def post(self, request, *args, **kwargs):
        items = request.POST.getlist("sortable-item")
        blocks = Block.objects.filter(homepage__tenant=self.request.user.tenant)
        if not len(blocks) == len(items):
            raise IndexError(_("Error matching sortables."))
        for weight, id in enumerate(items):
            item = blocks.get(id=id)
            item.weight = weight
            item.save()

        messages.success(self.request, "Blocks sucessfully reordered.")
        return HttpResponseRedirect(
            reverse_lazy("block-list", kwargs={"pk": self.get_object().pk})
        )


class BlockCreate(
    PermissionRequiredMixin, HtmxResponseMixin, ModalFormViewMixin, CreateView
):
    """Dynamic view for (polymorphic) Blocks"""

    permission_required = "homepage.change_homepage"
    fields = "__all__"
    homepage: Homepage = None
    block_type: str = ""
    success_event = "medux:homepage:block-added"

    def get_queryset(self):
        return self.homepage.blocks.all()

    def has_permission(self):
        self.block_type = self.kwargs.get("block_type")
        self.homepage = Homepage.objects.get(pk=self.kwargs.get("homepage_pk"))
        user = self.request.user
        return user.tenant == self.homepage.tenant and user.has_perms(
            self.get_permission_required()
        )

    def get_modal_title(self) -> str:
        return _("Add {model_name}").format(
            model_name=self.get_dynamic_model()._meta.verbose_name
        )

    def get_form_class(self) -> BlockForm:
        return all_forms[self.block_type]

    def get_dynamic_model(self):
        return self.get_form_class()._meta.model

    def get_template_names(self):
        """Allow overriding templates with ``homepage/block_<type>_form.html``"""
        return [
            f"homepage/block_{self.get_dynamic_model().__class__.__name__.lower()}_form.html",
            "homepage/block_form.html",
        ]

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        object_verbose_name = self.get_dynamic_model()._meta.verbose_name
        context.update(
            {
                "homepage": self.homepage,
                "object_verbose_name": object_verbose_name,
                "form": self.get_form(),
                "page_title": _("Create '{name}' block for {homepage}").format(
                    name=object_verbose_name, homepage=self.homepage
                ),
                "block_title": self.get_dynamic_model()._meta.verbose_name,
            }
        )

        return context

    def form_valid(self, form):
        block = form.save(commit=False)
        block.homepage = self.homepage
        block.save()
        # don't call super().form_valid here, as it would save the block again.
        return HttpResponseEmpty(headers={"HX-Trigger": self.success_event})


class BlockDetail(TenantPermissionRequiredMixin, DetailView):
    model = Block
    permission_required = "homepage.change_homepage"
    template_name = "homepage/block_detail_with_form.html"

    # def get_context_data(self, **kwargs):
    #     fields = []
    #     self.object = self.get_object()
    #     for fname in self.object._meta.get_fields():
    #         fields[fname] = getattr(self.object, fname)
    #     kwargs.update({"fields": fields})
    #     return super().get_context_data(**kwargs)


class BlockUpdate(TenantPermissionRequiredMixin, UpdateView):
    fields = "__all__"
    permission_required = "homepage.change_block"
    template_name = "homepage/block_detail_with_form.html"
    extra_context = {"edit": True}

    def get_success_url(self):
        return reverse_lazy("block-detail", kwargs={"pk": self.object.pk})

    def get_form_class(self) -> BlockForm:
        return all_forms[self.get_object().btype()]

    def get_queryset(self):
        return Block.objects.filter(homepage__tenant=self.request.user.tenant)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["block_title"] = self.object.verbose_name()
        return context


class BlockDelete(PermissionRequiredMixin, DeleteView):
    model = Block
    permission_required = "homepage.delete_block"

    def form_valid(self, form):
        self.object.delete()
        return HttpResponseEmpty(headers={"HX-Trigger": "medux:homepage:block-deleted"})
