from django.urls import reverse_lazy
from django.utils.translation import gettext_lazy as _

from medux.common.api.interfaces import IMenuItem


class Homepages(IMenuItem):
    menu = "views"
    title = _("Homepages")
    url = reverse_lazy("homepage:list")
    weight = 50
    icon = "pencil-square"
    required_permissions = "homepage.view_homepage"


class AddHomepage(IMenuItem):
    menu = "page_actions"
    title = _("Add Homepage")
    url = reverse_lazy("homepage:add")
    weight = 50
    icon = "plus-square"
    required_permissions = "homepage.add_homepage"
    view_name = "homepage:list"


class AddHomepageSiteSubMenu(IMenuItem):
    menu = "homepage"
    title = _("Homepage")
    url = "#"  # reverse_lazy("index")
    icon = "house"
    view_name = "tenant:detail"
    weight = 10
