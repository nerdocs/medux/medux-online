import turbo
from turbo.components import UserBroadcastComponent

from .models import Homepage


class HomepageStream(turbo.ModelStream):
    class Meta:
        model = Homepage

    # def on_save(self, homepage: Homepage, created: bool, *args, **kwargs):
    #     homepage.stream.update(
    #         text=homepage.name,
    #         # "homepage/components/homepage_title.html",
    #         # {"homepage": homepage},
    #         id=f"homepage-{homepage.pk}-title",
    #     )


# class BlockStream(turbo.ModelStream):
#     class Meta:
#         model = Block
#
#     def on_save(self, block: Block, created: bool, *args, **kwargs):
#         block.homepage.stream.remove(
#             id=f"block-detail-{{ block.pk }}",
#         )


class AlertComponent(UserBroadcastComponent):
    template_name = "homepage/components/alert.html"
