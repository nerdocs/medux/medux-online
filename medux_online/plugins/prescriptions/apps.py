from django.db.models.signals import post_save
from django.utils.translation import gettext_lazy as _

from medux.common.api import MeduxPluginAppConfig
from medux.common.signals import notify_user_from_model_instance_change
from medux.preferences.definitions import Scope, KeyType
from medux.preferences.registry import PreferencesRegistry
from . import __version__


class PrescriptionsConfig(MeduxPluginAppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "medux_online.plugins.prescriptions"
    default = True  # FIXME: Remove when django bug is fixed

    groups_permissions = {
        "Prescription editors": {
            "prescriptions.PrescriptionRequest": ["view", "add", "change", "delete"],
        }
    }

    class PluginMeta:
        """This configuration is the introspection data for plugins."""

        # the plugin machine "name" is taken from the AppConfig,
        # so no name here
        verbose_name = _("Prescriptions")
        author = "Christian González"
        author_email = "office@nerdocs.at"
        vendor = "Nerdocs"
        description = _("Online prescriptions management.")
        category = _("Base")
        visible = True
        version = __version__
        # compatibility = "medux_online.core>=2.3.0"

    def ready(self):
        try:
            from . import signals

            from medux_online.plugins.prescriptions.models import PrescriptionRequest

            post_save.connect(
                notify_user_from_model_instance_change, PrescriptionRequest
            )
        except ImportError:
            pass

        PreferencesRegistry.register(
            "prescriptions",
            "medication_message",
            [Scope.TENANT],
            KeyType.TEXT,
            icon="chat",
            help_text=_(
                "A message that is displayed to the user next to the prescription input field. "
                "It can show instructions about what prescriptions are allowed, or generic information."
            ),
        )
        PreferencesRegistry.register(
            "prescriptions",
            "show_medication_message",
            [Scope.TENANT],
            KeyType.BOOLEAN,
            icon="eye",
            help_text=_("Should the medication message be shown to the user?"),
        )
        PreferencesRegistry.register(
            "prescriptions",
            "use_approval",
            [Scope.TENANT],
            KeyType.BOOLEAN,
            icon="check-all",
            help_text=_(
                "Should prescription requests be approved by authorized personnel?"
            ),
        )
        PreferencesRegistry.register(
            "prescriptions",
            "thanks_message",
            [Scope.TENANT],
            KeyType.TEXT,
            icon="chat-left-dots",
            help_text=_(
                "The message that appears to the patient on the 'thanks'-page after "
                "submitting a prescription request"
            ),
        )
