from django.contrib import admin
from django.db.models import QuerySet
from django.utils.translation import gettext_lazy as _

from medux_online.plugins.prescriptions.models import (
    PrescriptionRequest,
    Medication,
)


class MedicationInline(admin.StackedInline):
    model = Medication
    extra = 3


@admin.action(description=_("Mark selected stories as approved"))
def approve(modeladmin, request, queryset: QuerySet[PrescriptionRequest]):
    queryset.update(approved=True)


@admin.register(PrescriptionRequest)
class PrescriptionRequestAdmin(admin.ModelAdmin):
    inlines = [MedicationInline]
    list_filter = ["tenant"]
    list_display = [
        "tenant",
        "first_name",
        "last_name",
        "insurance_number",
        "comment",
        "approved",
    ]
    actions = [approve]
