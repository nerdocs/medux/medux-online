from crispy_forms.bootstrap import FormActions
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Submit, Row, Field, Column, HTML
from django import forms
from django.forms import modelformset_factory
from django.utils.translation import gettext_lazy as _

# from medux_online.core.forms import TenantFormMixin
from medux_online.plugins.prescriptions.models import Medication, PrescriptionRequest

MedicationFormSet = modelformset_factory(
    model=Medication,
    fields=("name", "pkgs"),
    min_num=1,
)


class PrescriptionRequestForm(forms.ModelForm):  # TenantFormMixin
    class Meta:
        model = PrescriptionRequest
        fields = ["first_name", "last_name", "insurance_number", "comment"]

    insurance_number = forms.CharField(
        min_length=10,
        max_length=10,
        widget=forms.TextInput(attrs={"placeholder": "0000TTMMYY", "class": ""}),
        label=_("Insurance number"),
    )
    # password = forms.PasswordInput()

    # medication_formset = MedicationFormSet()
    comment = forms.CharField(
        widget=forms.Textarea(
            attrs={
                "placeholder": _("e.g.: \nAmlodipin 5mg, 1 pkg,\n2x Thyrex 100 ,\n...")
            }
        ),
        label=_("Medication"),
        help_text=_(
            "Please enter the exact medication names, dosages and package counts."
        ),
    )

    helper = FormHelper()
    helper.layout = Layout(
        Row(
            Column(
                Field("first_name", css_class="form-control-lg"),
                css_class="col-12 col-lg-4 responsive",
            ),
            Column(
                Field("last_name", css_class="form-control-lg"),
                css_class="col-12 col-lg-4 responsive",
            ),
            Column(
                Field("insurance_number", css_class="form-control-lg"),
                css_class="col-12 col-lg-4 responsive",
            ),
        ),
        Row(
            Column(
                HTML(
                    "{% if preferences.prescriptions.show_medication_message %}"
                    "<div class='alert alert-warning'>"
                    "{{ preferences.prescriptions.medication_message }}"
                    "</div>"
                    "{% endif %}"
                )
            )
        ),
        Row(
            Column(
                Field("comment", css_class="form-control-lg responsive", rows="5"),
                css_class="col-12 responsive",
            ),
        ),
        FormActions(
            Submit("submit", _("Send"), css_class="btn-prescription"),
        ),
    )
