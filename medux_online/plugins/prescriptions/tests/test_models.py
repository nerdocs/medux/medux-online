from medux.common.models import Tenant
from medux_online.core.tests import MeduxOnlineTestCase
from medux_online.plugins.prescriptions.models import PrescriptionRequest


class PrescriptionRequestModelTestCase(MeduxOnlineTestCase):
    def setUp(self) -> None:
        self.entry = PrescriptionRequest(
            first_name="John",
            last_name="Doe",
            insurance_number="2222222222",
            comment="Foo.",
            tenant=Tenant.objects.get(pk=1),
        )

    def test_string_representation(self):
        self.assertEqual(
            str(self.entry),
            f"{self.entry.last_name.upper()}, {self.entry.first_name},"
            f" {self.entry.insurance_number}",
        )

    def test_clean(self):
        entry = PrescriptionRequest(
            first_name=" John ",
            last_name="   Doe  ",
            insurance_number="   2222222222     ",
            comment="Foo.",
            tenant=Tenant.objects.get(pk=1),
        )
        entry.clean()
        self.assertEqual(entry.first_name, "John")
        self.assertEqual(entry.last_name, "Doe")
        self.assertEqual(entry.insurance_number, "2222222222")
