from medux_online.plugins.prescriptions.forms import PrescriptionRequestForm
from medux_online.plugins.prescriptions.tests import PrescriptionsTestCase


class PrescriptionRequestFormTest(PrescriptionsTestCase):
    def setUp(self) -> None:
        self.data = {
            "first_name": "John",
            "last_name": "Doe",
            "insurance_number": "2222222222",
            "comment": "Foo.",
        }

    def test_init(self):
        form = PrescriptionRequestForm()

    def test_add_prescriptionrequest(self):
        self.data["tenant"] = self.tenant1
        form = PrescriptionRequestForm(self.data)
        self.assertTrue(form.is_valid())
