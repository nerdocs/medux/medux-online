from django.urls import reverse

from medux_online.core.tests import MeduxOnlineTestCase


class PrescriptionRequestTest(MeduxOnlineTestCase):
    def test_try_to_request_prescriptions_for_admin_tenant(self):
        response = self.client.get(
            reverse(
                "prescriptionrequest-add", kwargs={"uuid": self.admin_tenant.uuid}
            )
        )
        # must return PermissionDenied
        self.assertEqual(response.status_code, 403)

    def test_try_to_request_prescriptions_for_valid_tenant(self):
        response = self.client.get(
            reverse("prescriptionrequest-add", kwargs={"uuid": self.tenant1.uuid})
        )
        self.assertEqual(response.status_code, 200)
