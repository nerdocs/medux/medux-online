from django.core.exceptions import ValidationError
from django.test import TestCase

from medux_online.plugins.prescriptions.models import validate_svnr


class TestSVNR(TestCase):
    def test_anumeric_svnr1(self):
        with self.assertRaises(ValidationError):
            validate_svnr("123456789o")

    def test_anumeric_svnr2(self):
        with self.assertRaises(ValidationError):
            validate_svnr("kjsafdhg")

    def test_svnr_wrong_len_tooshort(self):
        with self.assertRaises(ValidationError):
            validate_svnr("123456789")

    def test_svnr_len_toolong(self):
        with self.assertRaises(ValidationError):
            validate_svnr("12345678901")

    def test_svnr_startswith0(self):
        with self.assertRaises(ValidationError):
            validate_svnr("0123456789")

    def test_svnr_check_number(self):
        for i in [1, 2, 3, 4, 6, 7, 8, 9, 0]:
            with self.assertRaises(ValidationError):
                validate_svnr(f"123{i}010101")

    def test_svnr_day_wrong(self):
        with self.assertRaises(ValidationError):
            validate_svnr("2635321173")  # day=32
