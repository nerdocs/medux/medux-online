from django.contrib.auth.models import Group
from django.urls import reverse

from medux_online.plugins.prescriptions.models import (
    PrescriptionRequest,
)
from medux_online.plugins.prescriptions.tests import PrescriptionsTestCase


class PrescriptionViewsTest(PrescriptionsTestCase):
    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        site_editors = Group.objects.get(name="Prescription editors")
        cls.user1.groups.add(site_editors)

        cls.data = {
            "first_name": "John",
            "last_name": "Doe",
            "insurance_number": "2222222222",
            "comment": "Foo.",
        }

    # def test_dashboard_not_available_here(self):
    #     response = self.client.get("/dashboard/")
    #     self.assertEqual(response.status_code, 404)

    # def test_admin_not_available_here(self) -> None:
    #     response = self.client.get("/admin/")
    #     self.assertEqual(
    #         response.status_code,
    #         404,
    #         "Admin site must not be available on PrescriptionSites",
    #     )

    def test_prescriptionrequest_form_available(self):
        response = self.client.get(
            reverse("prescriptionrequest-add", kwargs={"uuid": self.tenant1.uuid}),
            self.data,
        )
        self.assertEqual(200, response.status_code)

    def test_create_prescriptionrequest(self):
        response = self.client.post(
            reverse("prescriptionrequest-add", kwargs={"uuid": self.tenant1.uuid}),
            self.data,
            follow=True,
        )
        self.assertRedirects(
            response,
            reverse(
                "prescriptionrequest-thanks", kwargs={"uuid": self.tenant1.uuid}
            ),
        )

        new_pr = PrescriptionRequest.objects.get(**self.data)
        for key in self.data:
            self.assertEqual(getattr(new_pr, key), self.data[key])

        self.assertEqual(new_pr.tenant, self.tenant1)
