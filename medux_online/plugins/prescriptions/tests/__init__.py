from medux_online.core.tests import MeduxOnlineTestCase


class PrescriptionsTestCase(MeduxOnlineTestCase):
    """A TestCase for usage with all prescription tests.

    It creates a basic prescriptions environment with a
    Prescriptions site named ``.example.com``.

    The ``site`` and ``server_name`` attributes are available within
    the class then.
    """

    site = None
