from django.urls import reverse_lazy
from django.utils.translation import gettext_lazy as _

from medux.common.api.interfaces import IMenuItem


class MedicationRequests(IMenuItem):
    menu = "views"
    title = _("Medication requests")
    url = reverse_lazy("prescriptionrequest:list")
    slug = "prescriptions_list"
    weight = 30
    icon = "list-check"
    # badge=NewPrescriptionsBadgeComponent
    required_permissions = "prescriptions.view_prescriptionrequest"


class VisitPrescriptionsForm(IMenuItem):
    menu = "page_actions"
    title = _("Visit prescriptions form")
    url = lambda request: reverse_lazy(
        "prescriptionrequest-add", args=(request.user.tenant.uuid,)
    )
    slug = "prescriptions_list"
    weight = 50
    icon = "list-check"
    # badge=NewPrescriptionsBadgeComponent,
    required_permissions = "prescriptions.view_prescriptionrequest"
    view_name = "prescriptionrequest:list"
    target = "_blank"
