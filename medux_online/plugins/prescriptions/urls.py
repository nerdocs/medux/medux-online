from django.urls import path, include

from medux_online.core.api.interfaces import IDashboardURL
from medux_online.plugins.prescriptions import views

app_name = "prescriptions"

urlpatterns = [
    # path("terms/", TermsView.as_view(), name="terms"),
    # path("privacy/", PrivacyView.as_view(), name="privacy"),
]

prescriptionrequest_patterns = [
    # request admin view is listed under /dashboard/requests/
    path("", views.PrescriptionRequestListView.as_view(), name="list"),
    path("<pk>/", views.PrescriptionRequestItemView.as_view(), name="detail"),
    path("<pk>/approve/", views.PrescriptionRequestApproveView.as_view(), name="approve"),
    path("<pk>/delete/", views.PrescriptionRequestDeleteView.as_view(), name="delete"),
]


class PrescriptionDashboardURLs(IDashboardURL):
    urlpatterns = [
        path(
            "prescriptionrequest/",
            include(
                (prescriptionrequest_patterns, "prescriptionrequest"),
                namespace="prescriptionrequest",
            ),
        )
    ]


# All following are patterns for generic medux.at URLs and not
# at a specific homepage, so they need a tenant_id
root_urlpatterns = [
    path(
        "tenant/<uuid:uuid>/prescriptionrequests/add",
        views.PrescriptionRequestCreateView.as_view(),
        name="prescriptionrequest-add",
    ),
    path(
        "tenant/<uuid:uuid>/prescriptionrequests/thanks/",
        views.ThanksView.as_view(),
        name="prescriptionrequest-thanks",
    ),
]
