from django.http import HttpRequest

from medux_online.core.models import User
from medux_online.plugins.prescriptions.models import PrescriptionRequest


def get_prescriptionrequest_count(request_or_user: User | HttpRequest) -> int:
    """Helper function for badge

    :return: count of open/unresolved prescription requests of current
        tenant
    """
    assert isinstance(request_or_user, (User, HttpRequest))
    return PrescriptionRequest.objects.filter(tenant=request_or_user.tenant).count()
