import turbo
from medux.common.streams import MenuItemBadgeComponent
from turbo.components import BaseComponent

from medux.common.models import Tenant
from .models import PrescriptionRequest
from .utils import get_prescriptionrequest_count


class NewPrescriptionsBadgeComponent(MenuItemBadgeComponent):
    def __init__(self, user):
        super().__init__(user)

    def get_badge_content(self) -> str:
        return str(get_prescriptionrequest_count(self.user))

    # def get_badge_title(self) -> str:
    #     return super().get_badge_title()

    def user_passes_test(self, user) -> bool:
        return user.has_perm("prescriptions.view_prescriptionrequest")


class TenantStream(turbo.ModelStream):
    class Meta:
        model = Tenant

    def user_passes_test(self, user):
        return user and user.is_authenticated and user.tenant == self.instance.tenant


class PrescriptionRequestStream(turbo.ModelStream):
    """A ModelStream that keeps track of :class:`PrescriptionRequest`s.

    It notifies all logged in users of the PR's tenant of the change
    via :class:`medux.plugins.prescriptions.streams.TenantStream`."""

    class Meta:
        model = PrescriptionRequest

    def on_delete(self, instance: PrescriptionRequest, *args, **kwargs) -> None:
        TenantStream(instance.tenant).remove(id=f"request-item-{instance.id}")

    def on_save(
        self, instance: PrescriptionRequest, created: bool, *args, **kwargs
    ) -> None:
        if created:
            TenantStream(instance.tenant).append(  # noQa
                "prescriptions/components/prescription_request_item.html",
                {"object": instance, "perms": self},
                id="request-items",
            )


class DeletePrescriptionrequestButton(BaseComponent):
    template_name = "prescriptions/components/delete_prescriptionrequest_button.html"
