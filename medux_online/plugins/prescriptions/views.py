from django.contrib import messages
from django.contrib.auth.mixins import PermissionRequiredMixin
from django.core.exceptions import PermissionDenied
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.utils.translation import gettext_lazy as _
from django.views.generic import (
    CreateView,
    TemplateView,
    ListView,
    DetailView,
    DeleteView,
)

from medux.common.api.http import HttpResponseEmpty
from medux.common.htmx.mixins import HtmxResponseMixin
from medux.common.mixins import TenantPermissionRequiredMixin
from medux.common.models import Tenant
from medux.common.views import DashboardMixin
from medux.preferences.cached_preferences import CachedPreferences
from medux.preferences.definitions import Scope
from medux_online.core.views import TenantRequestMixin
from medux_online.core.models import User
from medux_online.plugins.prescriptions.forms import PrescriptionRequestForm
from medux_online.plugins.prescriptions.models import PrescriptionRequest


class PrescriptionRequestListView(
    PermissionRequiredMixin, TenantRequestMixin, DashboardMixin, ListView
):
    """Shows a list of prescription requests, and some editing actions."""

    model = PrescriptionRequest
    template_name = "prescriptions/prescription_request_list.html"
    permission_required = "prescriptions.view_prescriptionrequest"
    paginate_by = 10
    ordering = ["created"]


class PrescriptionRequestItemView(
    PermissionRequiredMixin, HtmxResponseMixin, DetailView
):
    model = PrescriptionRequest
    template_name = "prescriptions/prescription_request_item.html"
    permission_required = "prescriptions.view_prescriptionrequest"
    enforce_htmx = True

    def has_permission(self):
        user: User = self.request.user
        # self.object.tenant == user.tenant and
        if not type(self.permission_required) == list:
            self.permission_required = [self.permission_required]
        return user.has_perms(self.permission_required)


class PrescriptionRequestApproveView(
    TenantPermissionRequiredMixin, HtmxResponseMixin, DetailView
):
    model = PrescriptionRequest
    permission_required = "prescriptions.approve_prescriptionrequest"
    enforce_htmx = True


class PrescriptionRequestDeleteView(
    TenantPermissionRequiredMixin, HtmxResponseMixin, DeleteView
):
    model = PrescriptionRequest
    permission_required = "prescriptions.delete_prescriptionrequest"
    enforce_htmx = True

    def form_valid(self, form):
        # self.object.delete()
        messages.success(
            self.request, "Prescription request deleted.", extra_tags="dismissible"
        )
        return HttpResponseEmpty(
            headers={"HX-Trigger": "medux:prescriptionrequest:deleted"}
        )


class PrescriptionRequestCreateView(PermissionRequiredMixin, CreateView):
    """Let's anonymous users create prescription requests in a form."""

    model = PrescriptionRequest
    form_class = PrescriptionRequestForm
    template_name = "prescriptions/prescriptionrequest_form.html"

    def has_permission(self):
        """Don't allow prescription requests on MedUX main site admin."""

        if self.request.site.pk != 1:
            raise PermissionDenied(
                _("There are no prescription requests allowed on this site.")
            )
        return True

    def get_success_url(self):
        return reverse(
            "prescriptionrequest-thanks",
            kwargs={"uuid": self.kwargs.get("uuid")},
        )

    def get_context_data(self, **kwargs):
        """Add url's tenant to context"""
        context = super().get_context_data(**kwargs)
        tenant = Tenant.objects.get(uuid=self.kwargs.get("uuid"))
        if tenant.id == 1:
            # Admin does not use prescriptions...
            raise PermissionDenied
        context.update({"tenant": tenant})
        return context

    def form_valid(self, form):
        prescription_request = form.save(commit=False)
        prescription_request.tenant = Tenant.objects.get(uuid=self.kwargs.get("uuid"))
        prescription_request.save()
        return HttpResponseRedirect(self.get_success_url())


class ThanksView(TemplateView):
    template_name = "prescriptions/thanks.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        tenant = Tenant.objects.get(uuid=self.kwargs.get("uuid"))

        thanks_message = CachedPreferences.get(
            "prescriptions", "thanks_message", Scope.TENANT, tenant=tenant
        ) or CachedPreferences.get("prescriptions", "thanks_message", Scope.VENDOR)

        context.update({"tenant": tenant, "thanks_message": thanks_message})
        return context
