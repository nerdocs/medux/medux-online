import logging

logger = logging.getLogger(__file__)


class TenantMiddleware:
    """A middleware that adds the current site's tenant to the request object.

    Add `medux_online.core.middleware.TenantMiddleware` to your MIDDLEWARE
    dict in settings.py, after CurrentSiteMiddleware.
    You can use the tenant in a template to your needs:

    In a template:

        <span>Tenant: {{ request.tenant }}</span>

    Or in a view:

        print(request.tenant)
    """

    def __init__(self, get_response):
        """Initializes the middleware"""
        self.get_response = get_response

    def __call__(self, request):
        """Add current site's Tenant as `tenant` attribute to the
        current request."""

        if hasattr(request.site, "homepage"):
            request.tenant = request.site.homepage.tenant
            logger.debug(f"Requesting page from host {request.site.homepage}.")
        else:
            request.site.tenant = None

        response = self.get_response(request)
        return response


class HomepageMiddleware:
    """
    Middleware that sets `homepage` attribute to request object.
    Put this after ``sites.CurrentSiteMiddleware``.
    """

    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        """Add current site's Homepage as ``homepage`` attribute to the
        current request."""

        if hasattr(request.site, "homepage"):
            request.homepage = request.site.homepage
            logger.debug(f"Requesting page from homepage {request.homepage}.")
        else:
            request.homepage = None
            logger.debug(f"Requesting page from primary site {request.site}.")

        response = self.get_response(request)
        return response
