from django.utils.translation import gettext_lazy as _

from medux.preferences.definitions import Scope, ScopeIcons
from medux.preferences.models import ScopedPreference


def create_filter(request, scope: Scope):
    """Creates a filter dict that can be used as parameter for
    filtering a Queryset."""
    if not type(scope) == Scope:
        scope = Scope[scope]
    scope_name = scope.name.lower()
    if scope_name == "group":
        filter = {f"{scope_name}__in": request.user.groups}
    else:
        filter = {scope_name: getattr(request, scope_name)}
    return filter


def has_change_own_scopedpreference_perm(user, setting: ScopedPreference) -> bool:
    """Check that given user has permission to change given ScopedPreference.

    It makes sure the user's tenant matches the setting's, and the user hast
    the needed change_own_{scope_name}_scopedpreferences permission.
    Vendor settings are immutable, here it returns always ``False``.
    """
    match setting.scope:
        case Scope.VENDOR:
            # never! change vendor settings.
            return False
        case Scope.DEVICE:
            return False  # FIXME: remove when device is implemented
            # user can only change device settings of own tenant's devices
            object_matches = user.tenant == setting.device.tenant
        case Scope.TENANT:  #
            # user can only change tenant settings of own tenant
            object_matches = user.tenant == setting.tenant
        case Scope.GROUP:
            # user can only change group settings of groups he is in.
            object_matches = setting.group in user.groups.all()
        case Scope.USER:  #
            # user can only change his own user settings
            object_matches = user == setting.user

    return object_matches and user.has_perm(
        f"preferences.change_own_{Scope(setting.scope).name.lower()}_scopedpreference"
    )


def has_perm_with_tenant(user, obj, permission: str) -> bool:
    """Chech that given user has given permission, and his tenant matches the obj's.
    :return: True if permissions and tenant match, False if not"""
    return user.tenant == obj.tenant and user.has_perm(permission)


def create_scope_context_object(scope) -> dict:
    """:return: a usable scope dict for templates,
    which contains the scope icon too."""
    if type(scope) == str:
        scope = Scope[scope.upper()]
    elif type(scope) == int:
        scope = Scope(scope)
    return {
        "name": scope.name,
        "value": scope.value,
        "icon": ScopeIcons[scope.name].value,
        "display_name": _(scope.name.capitalize()),
    }
