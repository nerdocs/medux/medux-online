from django.contrib.sites.models import Site
from django.db import models
from django.urls import reverse
from django.utils.translation import gettext_lazy as _

from medux.common.models import TenantModelMixin
from medux.common.models import CommonUser


class User(CommonUser):
    """The MedUX Online user class"""


class SiteMixin(models.Model):
    """Mixin that declares a ForeignKey to a Site in the model.

    Models with this Mixin belong to a certain Site.
    """

    class Meta:
        abstract = True

    site = models.ForeignKey(Site, on_delete=models.CASCADE)

    def get_absolute_url(self):
        return reverse("homepage:update", kwargs={"pk": self.pk})
        # FIXME: dashboard is not available here...

    @property
    def slug(self):
        return self.domain.replace(".", "_")


class TermsAndConditionsPage(TenantModelMixin, models.Model):
    class Meta:
        verbose_name = _("Terms and conditions page")
        verbose_name_plural = _("Terms and conditions page")

    # TODO: use django-versionfield
    version = models.CharField(max_length=15)

    # TODO: use django-markdownfield etc.
    content = models.TextField()

    def __str__(self):
        return self.version


class PrivacyPage(TenantModelMixin, models.Model):
    class Meta:
        verbose_name = _("Privacy page")
        verbose_name_plural = _("Privacy pages")

    # TODO: use django-versionfield
    version = models.CharField(max_length=15)

    # TODO: use django-markdownfield etc.
    content = models.TextField()

    def __str__(self):
        return self.version
