from django.contrib.auth.mixins import AccessMixin


class HomepageRequiredMixin(AccessMixin):
    """Mixin that verifies we are on a Homepage site type."""

    def dispatch(self, request, *args, **kwargs):
        # TODO: maybe don't raise an Error, but a 403
        if self.request.homepage is None:
            raise PermissionError(
                f"The view {self.__class__.__name__} can't be accessed from the host {self.request.site}."
            )

        return super().dispatch(request, *args, **kwargs)


# class SpecialHomepageRequiredMixin(AccessMixin):
#     """Mixin that verifies we are on a Homepage site type."""
#
#     def is_correct_site(self):
#         """ """
#         if self.request.homepage is None:
#             # TODO: print proper error message
#             raise ImproperlyConfigured(
#                 f"{self.__class__.__name__} is missing the site_model attribute. Define "
#                 f"{self.__class__.__name__}.site_model."
#             )
#         if not issubclass(self.site_model, Homepage):
#             return False
#         try:
#             self.site_model.objects.get(pk=self.request.site.id)
#             return True
#         except Homepage.DoesNotExist:
#             return False
#
#     def dispatch(self, request, *args, **kwargs):
#         # TODO: maybe don't raise an Error, but a 403
#         if not self.is_correct_site():
#             raise PermissionError(
#                 f"The view {self.__class__.__name__} can't be accessed from the host {self.request.site}."
#             )
#
#         return super().dispatch(request, *args, **kwargs)
