from django.urls import reverse_lazy
from django.utils.translation import gettext_lazy as _

from medux.common.api.interfaces import IMenuItem
from medux.preferences.definitions import ScopeIcons


class Preferences(IMenuItem):
    menu = "user"
    title = _("Preferences")
    url = reverse_lazy("preferences:overview")
    weight = 50
    icon = "list"
    check = lambda request: request.user.is_authenticated


class Login(IMenuItem):
    menu = "user"
    title = _("Login")
    url = reverse_lazy("login")
    weight = 90
    icon = "box-arrow-in-right"
    check = lambda request: request.user.is_anonymous


class AdministrationMenu(IMenuItem):
    menu = "views"
    slug = "administration"
    title = _("Administration")
    url = "#"
    weight = 90
    icon = "gear"
    check = lambda request: request.user.is_superuser
    collapsed = False


class TenantAdministration(IMenuItem):
    # FIXME: this item is shown to unprivileged users
    menu = "views"
    slug = "administration.tenant"
    title = _("Tenants")
    url = reverse_lazy("tenant:list")
    weight = 50
    icon = ScopeIcons.TENANT.value
    required_permissions = "common.change_tenant"


class UserAdministration(IMenuItem):
    menu = "views"
    slug = "administration.user"
    title = _("User")
    weight = 30
    icon = "people"
    url = reverse_lazy("user:list")
    required_permissions = "common.change_user"


class UserProfile(IMenuItem):  # FIXME: rename to settings, or make Profile MenunItem
    exact_url = True
    menu = "user"
    title = _("Edit profile")
    weight = 50
    icon = "user"
    url = reverse_lazy("user:profile")
    check = lambda request: request.user.is_authenticated


class AddUserAction(IMenuItem):
    menu = "page_actions"
    title = _("Add user")
    icon = "person-add"
    url = reverse_lazy("user:add")
    view_name = "user:list"
    required_permissions = "core.add_user"


class AddTenantAction(IMenuItem):
    menu = "page_actions"
    title = _("Add Tenant")
    url = reverse_lazy("tenant:add")
    weight = 50
    icon = "list-check"
    required_permissions = "core.add_tenant"
    view_name = "tenant:list"


# class UserProfileSectionMasterData(IMenuItem):
#     """Master data section (name, email etc.) of user profile"""
#
#     menu = "user_profile_sections"
#     url = reverse_lazy("user:profile:master-data")
#     title = _("Master data")
#     icon = "person"
#
#
# class UserProfileSectionPassword(IMenuItem):
#     """Password section of user profile"""
#
#     menu = "user_profile_sections"
#     url = reverse_lazy("user:profile:password")
#     title = _("Password")
#     icon = "braces-asterisk"
#
#
# class UserProfileSectionGroups(IMenuItem):
#     """Groups section of user profile"""
#
#     menu = "user_profile_sections"
#     url = reverse_lazy("user:profile:groups")
#     title = _("Groups")
#     icon = "people"
