from gdaps.api import Interface
from gdaps.api.interfaces import ITemplatePluginMixin


# TODO get rid of dynamic dashboard URL generating
@Interface
class IDashboardURL:
    """This interface offers a urlpattern that is included dynamically
    into the dashboard.

    Add an IDashBoardURL implementation to your urls.py, and its
    urlpatterns will show up in the dashboard automatically.
    """

    urlpatterns = []


@Interface
class ITenantDetailAddon(ITemplatePluginMixin):
    """Plugin hook that is rendered on the tenant:detail page
    after the main detail card and shows additional data there."""
