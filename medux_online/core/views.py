import logging

from django.contrib import messages
from django.contrib.auth import authenticate
from django.contrib.auth.mixins import PermissionRequiredMixin
from django.contrib.messages.views import SuccessMessageMixin
from django.core.exceptions import ValidationError, PermissionDenied
from django.core.validators import validate_integer
from django.db import models
from django.db.models import Q
from django.http import (
    HttpRequest,
    HttpResponseForbidden,
)
from django.shortcuts import render, redirect, get_object_or_404
from django.urls import reverse, reverse_lazy
from django.utils.decorators import method_decorator
from django.utils.translation import gettext as _
from django.views.decorators.csrf import csrf_protect
from django.views.decorators.debug import sensitive_post_parameters
from django.views.generic import (
    TemplateView,
    ListView,
    DetailView,
    UpdateView,
    DeleteView,
    CreateView,
)

import medux.common.forms
import medux_online.core.forms.preferences
import medux_online.core.forms.tenant
import medux_online.core.forms.user
from medux.common.api.http import HttpResponseEmpty
from conjunto.api.interfaces import ISettingsSection, ModalFormViewMixin
from medux.common.forms import (
    UserProfileMasterDataForm,
    UserProfileGroupsForm,
    UserProfilePasswordForm,
)
from medux.common.htmx.mixins import HtmxResponseMixin
from medux.common.models import Tenant
from medux.common.views import (
    FormActionsMixin,
    DashboardMixin,
)
from medux.preferences.definitions import Scope
from medux.preferences.models import ScopedPreference, BasePreference
from medux.preferences.registry import PreferencesRegistry
from medux.preferences.tools import (
    is_effective,
    is_editable,
    is_deletable,
)
from medux_online.core.forms import ScopedPreferenceForm
from medux_online.core.models import TermsAndConditionsPage, PrivacyPage, User
from medux_online.core.tools import (
    has_change_own_scopedpreference_perm,
    create_scope_context_object,
)

logger = logging.getLogger(__file__)


class SignUpView(SuccessMessageMixin, CreateView):
    template_name = "registration/signup.html"
    success_url = reverse_lazy("login")
    success_message = _("Your profile was created successfully.")
    form_class = medux.common.forms.SignUpForm

    def form_valid(self, form):
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get("username")
            password = form.cleaned_data.get("password1")
            user = authenticate(username=username, password=password)


class DashboardView(DashboardMixin, PermissionRequiredMixin, TemplateView):
    template_name = "common/page.html"

    def has_permission(self):
        if self.request.site.id != 1:
            raise PermissionDenied(
                f"Sorry, the dashboard is not accessible on {self.request.site.domain}."
            )
        return self.request.user.is_authenticated


class TenantURLMixin:
    """A mixin that enables a view which has a tenant's uuid in the URL to be tenant aware.

    It filters the view's queryset using the given tenant
    and adds ``tenant`` to the template's context.
    """

    def get_queryset(self):
        """Filter the query by the current tenant."""
        return self.model._default_manager.filter(tenant__uuid=self.kwargs.get("uuid"))

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update({"tenant": Tenant.objects.get(uuid=self.kwargs.get("uuid"))})
        return context


class TenantRequestMixin:
    """A mixin that filters a ModelView's queryset by the current
    request's user's tenant.

    It also adds ``tenant`` to the template's context.
    """

    def get_queryset(self):
        """Filter the query by the current tenant."""
        return self.model._default_manager.filter(tenant=self.request.user.tenant)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update({"tenant": self.request.user.tenant})
        return context


class TenantStaticPageView(TenantURLMixin, TemplateView):
    """Tenant-aware base view for static pages like Terms / Privacy, etc.

    You specify a model which will be rendered as page. This model must
    contain a `title`, `version`, and `content` attribute.
    The latest version found is rendered.
    The `title` attribute is also used as page title.

    You can override the `homepage/staticpage.html` template.
    """

    class Meta:
        abstract = True

    model: type(models.Model) = None
    title: str = None

    template_name = "core/staticpage.html"

    def get(self, request: HttpRequest, **kwargs):
        # noinspection PyUnresolvedReferences
        obj = self.get_queryset().order_by("version").last()
        # context = {"title": self.title, "object": object}
        return super().get(
            request,
            template_name=self.template_name,
            title=self.title,
            object=obj,
            **kwargs,
        )


class PrivacyView(TenantStaticPageView):
    model = PrivacyPage
    title = _("Privacy")


class TermsView(TenantStaticPageView):
    model = TermsAndConditionsPage
    title = _("Terms and conditions")


class TenantListView(PermissionRequiredMixin, DashboardMixin, ListView):
    template_name = "core/tenant_list.html"
    model = Tenant
    permission_required = "core.view_tenant"


class TenantCreateView(PermissionRequiredMixin, DashboardMixin, CreateView):
    template_name = "core/tenant_form.html"
    model = Tenant
    form_class = medux_online.core.forms.tenant.TenantForm
    permission_required = "core.add_tenant"

    success_url = reverse_lazy("tenant:list")


class TenantUpdateView(
    PermissionRequiredMixin, DashboardMixin, FormActionsMixin, UpdateView
):
    template_name = "core/tenant_form.html"
    form_class = medux_online.core.forms.tenant.TenantForm
    model = Tenant
    permission_required = "core.change_tenant"
    success_url = reverse_lazy("tenant:list")

    def delete(self, request, *args, **kwargs):
        return redirect(
            reverse_lazy("tenant:delete", kwargs={"pk": self.get_object().pk})
        )


class TenantDeleteView(PermissionRequiredMixin, DashboardMixin, DeleteView):
    model = Tenant
    permission_required = "core.delete_tenant"
    template_name = "common/object_confirm_delete.html"
    success_url = reverse_lazy("tenant:list")

    def has_permission(self):
        """Never let the user delete #1 Admin."""
        return super().has_permission() and not self.get_object().pk == 1


class BasePreferenceListView(PermissionRequiredMixin, DashboardMixin, ListView):
    """Shows a list of (Base)Settings and highlights their effective ScopedPreference."""

    # TODO: base this View on BasePreference model
    model = ScopedPreference
    permission_required = ["preferences.view_scopedpreference"]
    template_name = "preferences/overview.html"
    # TODO: fix ordering = ["base__namespace", "base__key"]

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        # monkey-patch effectiveness of each setting due to request
        for setting in self.object_list:
            setting.effective = is_effective(setting, self.request)
        context.update(
            {
                "namespaces": ScopedPreference.namespaces(),
                "keys": ScopedPreference.keys(),
            }
        )
        return context


def validate_bool(value):
    if not value in (True, False):
        raise ValidationError(
            "Value is not a Boolean: {value}", code="invalid", params={"value": value}
        )


def validate_setting(setting_obj: ScopedPreference):
    key_type = PreferencesRegistry.key_type(
        setting_obj.base.namespace, setting_obj.base.key
    )
    match key_type:
        case "int":
            validate_integer(setting_obj.value)
        case "str" | "text":
            pass
        case "bool":
            validate_bool(setting_obj.value)


class BasePreferenceDetailView(PermissionRequiredMixin, DashboardMixin, DetailView):
    model = BasePreference
    preferences = ScopedPreference.objects.none()
    permission_required = "preferences.view_scopedpreference"

    def get(self, request, *args, **kwargs):
        self.object: BasePreference = self.get_object()
        all = self.object.scopedpreferences.all()

        # FIXME: https://code.djangoproject.com/ticket/20024
        # self.scopedpreferences = self.object.scopedpreferences.filter(
        #     tenant__in=[request.user.tenant, None]
        # ).order_by("base__namespace", "base__key")
        self.preferences = self.object.scopedpreferences.filter(
            Q(tenant=request.user.tenant) | Q(tenant=None)
        ).order_by("base")

        # monkey-patch effectiveness and editable/deletable permission
        # into this queryset's objects
        for item in self.preferences:
            item.effective = is_effective(item, self.request)
            item.editable = is_editable(item, self.request)
            item.deletable = is_deletable(item, self.request)

        all_scopes = PreferencesRegistry.scopes(self.object.namespace, self.object.key)
        saved_scopes = [item.scope for item in self.preferences]
        available_scopes = []
        for scope in all_scopes:
            if not scope in saved_scopes and self.request.user.has_perm(
                f"preferences.change_own_{scope.name.lower()}_scopedpreference"
            ):
                available_scopes.append(scope)
        # form = ScopedPreferenceForm(queryset=self.object_list)
        context = self.get_context_data()
        context.update(
            {
                # "form": form,
                "scopedpreferences": self.preferences,
                "scopes": [create_scope_context_object(scope) for scope in all_scopes],
                "available_scopes": [
                    create_scope_context_object(scope) for scope in available_scopes
                ],
            }
        )
        return self.render_to_response(context)


class UserListView(PermissionRequiredMixin, DashboardMixin, ListView):
    model = User
    permission_required = "core.change_user"


class UserCreateView(PermissionRequiredMixin, DashboardMixin, CreateView):
    model = User
    permission_required = "core.add_user"
    form_class = medux.common.forms.UserCreationForm
    template_name = "common/user_create_form.html"
    success_url = reverse_lazy("user:list")

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs.update({"user": self.request.user})
        return kwargs


class UserDeleteView(PermissionRequiredMixin, DashboardMixin, DeleteView):
    model = User
    permission_required = "common.delete_user"
    template_name = "common/object_confirm_delete.html"
    success_url = reverse_lazy("user:list")

    def has_permission(self):
        """Never let the user delete #1 Admin."""
        return super().has_permission() and not self.get_object().pk == 1


############# ScopedPreference #############


class ScopedPreferenceCreateView(
    PermissionRequiredMixin, ModalFormViewMixin, HtmxResponseMixin, CreateView
):
    model = ScopedPreference
    enforce_htmx = False
    form_class = ScopedPreferenceForm
    success_event = "medux:scopedpreference:added"

    def has_permission(self):
        # TODO check for permissions here
        return self.request.user.is_authenticated

    def get_modal_title(self):
        return _("New {scope} setting for '{preference}'").format(
            scope=_(Scope[self.kwargs.get("scope").upper()].name.capitalize()),
            preference=self.base,
        )

    def post(self, request, *args, **kwargs):
        self.base = get_object_or_404(BasePreference, pk=self.kwargs.get("base_pk"))
        return super().post(request, *args, **kwargs)

    def get(self, request, *args, **kwargs):
        self.object = None
        scope = Scope[self.kwargs.get("scope").upper()]
        self.base = get_object_or_404(BasePreference, pk=self.kwargs.get("base_pk"))

        for setting in ScopedPreference.objects.filter(base=self.base, scope=scope):
            setting_already_there = False
            match scope:
                case Scope.TENANT:
                    if setting.tenant == request.user.tenant:
                        setting_already_there = True
                # TODO: case Scope.GROUP:
                #     if setting.group ...
                # TODO: case Scope.DEVICE:
                #     if setting.device ...
                case Scope.USER:
                    if setting.user == request.user:
                        setting_already_there = True

            if setting_already_there:
                return HttpResponseForbidden(
                    "There is already a setting with this scope."
                )
        return self.render_to_response(
            self.get_context_data(**kwargs),
        )

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs.update(
            {
                "scope": self.kwargs.get("scope"),
                "base": self.base,
                "request": self.request,
            }
        )
        return kwargs

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update(
            {
                "base": self.base,
                "scope": create_scope_context_object(
                    self.kwargs.get("scope"),
                ),
            }
        )
        return context


class ScopedPreferenceDetailView(PermissionRequiredMixin, DetailView):
    model = ScopedPreference
    template_name = "preferences/scopedpreference_item.html"
    permission_required = "preferences.view_scopedpreference"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update({"editable": is_editable(self.object, self.request)})
        return context


def scopedpreference_update(request, pk):
    obj = get_object_or_404(ScopedPreference, pk=pk)
    scope = Scope(obj.scope)
    form = medux_online.core.forms.preferences.ScopedPreferenceForm(
        data=request.POST or None,
        instance=obj,
        scope=scope,
        request=request,
    )
    context = {
        "base_object": obj.base,
        "object": obj,
        "scope": create_scope_context_object(scope),
    }
    if scope == Scope.VENDOR:
        return HttpResponseForbidden("A vendor setting may not be edited.")

    if request.method == "POST":
        if form.is_valid():
            form.save()
            return redirect("preferences:base-detail", pk=obj.base.pk)

    context.update({"form": form})
    return render(request, "preferences/scopedpreference_form.html", context)


class ScopedPreferenceDeleteView(PermissionRequiredMixin, DeleteView):
    model = ScopedPreference
    base_pk = None
    object = None

    def has_permission(self):
        self.object = self.get_object()
        self.base_pk = self.object.base.pk
        return has_change_own_scopedpreference_perm(self.request.user, self.object)

    def get_success_url(self):
        return reverse("preferences:base-detail", args=[self.base_pk])


def set_active_tenant(request, pk):
    """Sets active tenant to current user (if he is staff member)"""
    if not request.user.is_staff:
        return HttpResponseForbidden("Not allowed for non-staff users.")

    tenant = get_object_or_404(Tenant, pk=pk)
    user = request.user
    user.tenant = tenant
    user.save()
    return redirect(request.GET.get("next"))


class UserProfileMasterDataView(  # FIXME: rename to Settings...
    ISettingsSection, PermissionRequiredMixin, UpdateView
):
    name = "master_data"
    title = _("Master data")
    icon = "person"
    model = User
    form_class = UserProfileMasterDataForm

    def form_valid(self, form):
        # call super() methods, but return an empty response
        response = super().form_valid(form)
        messages.info(self.request, _("User master data saved."))
        return HttpResponseEmpty()


class UserProfilePasswordView(ISettingsSection, PermissionRequiredMixin, UpdateView):
    form_class = UserProfilePasswordForm
    name = "password"
    title = _("Password")
    icon = "braces-asterisk"
    model = User

    @method_decorator(sensitive_post_parameters())
    @method_decorator(csrf_protect)
    def dispatch(self, *args, **kwargs):
        return super().dispatch(*args, **kwargs)

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        # kwargs["user"] = self.request.user
        # kwargs.update({"instance": self.request.user})
        return kwargs

    def form_valid(self, form):
        user = form.save()
        # Updating the password logs out all other sessions for the user
        # except the current one.
        # FIXME: update_session_auth_hash only works for the HTMX request,
        #  not the calling one
        # update_session_auth_hash(self.request, user)
        # return super().form_valid(form)
        if user == self.request.user:
            # log out user
            return HttpResponseEmpty(headers={"HX-Redirect": reverse_lazy("login")})
        else:
            # I changed another users PW as admin, so don't log me out...
            super().form_valid(form)
            messages.info(self.request, _("Password saved."))
            return HttpResponseEmpty()

    def get_context_data(self, **kwargs):
        """As PasswordChangeView does not inherit from any ModelView,
        there is no `object` available in template. This method does that.
        """
        kwargs = super().get_context_data()
        kwargs["object"] = User.objects.get(pk=self.kwargs.get("pk"))
        return kwargs


class UserProfileGroupsView(ISettingsSection, PermissionRequiredMixin, UpdateView):
    name = "groups"
    title = _("Groups")
    icon = "people"
    model = User
    form_class = UserProfileGroupsForm

    def form_valid(self, form):
        # call super() methods, but return an empty response
        response = super().form_valid(form)
        messages.info(self.request, _("Groups saved."), extra_tags="dismissible")
        return HttpResponseEmpty()
