from django.conf import settings
from django.contrib import admin
from django.contrib.auth.models import Permission
from django.utils.translation import gettext_lazy as _

from medux_online.core.models import User


class PermissionAdmin(admin.ModelAdmin):
    search_fields = ["name", "codename"]


admin.site.site_header = _("{project_title} administration").format(
    project_title=settings.PROJECT_TITLE
)
admin.site.site_title = settings.PROJECT_TITLE
admin.site.index_title = _("Medically welcome to {project_title}").format(
    project_title=settings.PROJECT_TITLE
)

admin.site.register(Permission, PermissionAdmin)
admin.site.register(User)
