from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Row, Column, Submit
from django import forms
from django.utils.translation import gettext_lazy as _

from medux.common.models import Tenant, SexChoices


# TODO: TenantFormFixin to enable tenant aware generic forms.
# class TenantFormMixin:
#     """A mixin that adds the current request's tenant to the form data.
#
#     Make sure the view using this form has :class:`TenantViewMixin` included.
#     """
#
#     def __init__(self, *args, **kwargs):
#         self.tenant = kwargs["initial"].pop("tenant")
#         super().__init__(self, *args, **kwargs)
#
#     def clean(self):
#         super().clean()
#         self.cleaned_data["tenant_id"] = self.tenant.id


class TenantForm(forms.ModelForm):
    class Meta:
        model = Tenant
        exclude = ["uuid"]

    sex = forms.ChoiceField(
        label=_("Sex"),
        choices=SexChoices.choices,
        # widget=RadioSelectButtonGroup,
    )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper(self)
        self.helper.layout = Layout(
            Row(Column("title"), Column("first_name"), Column("last_name")),
            Row(
                Column("sex"),  # Field("sex", css_class="btn-check")
                Column("address"),
                Column("phone"),
                Column("email"),
            ),
        )
        self.helper.add_input(Submit("submit", _("Save")))
        if self.instance.pk:
            self.helper.add_input(Submit("delete", _("Delete"), css_class="btn-danger"))
