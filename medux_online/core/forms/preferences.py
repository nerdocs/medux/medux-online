from typing import List

from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Field
from django import forms
from django.db.models import TextChoices
from django.utils.translation import gettext_lazy as _

from medux.common.models import Tenant
from medux.common.tools import snake_case_to_spaces, str_to_bool
from medux.common.widgets import BooleanTextFieldButtonsGroup
from medux.preferences.definitions import ScopeIcons, Scope, KeyType
from medux.preferences.models import ScopedPreference
from medux.preferences.registry import PreferencesRegistry
from medux.preferences.tools import get_effective_setting


class PreferencesForm(forms.Form):
    """Dynamic form that creates fields from ScopedPreference values."""

    def get_context(self):
        context = super().get_context()
        fields = context["fields"]  # type: List[forms.Field]
        object_list = {}
        for field, safestr in fields:
            namespace, key, scope = field.name.split("__")
            if namespace not in object_list:
                object_list[namespace] = {}
            if key not in object_list[namespace]:
                object_list[namespace][key] = {}
            object_list[namespace][key][scope.upper()] = field

        context["object_list"] = object_list
        return context

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        for namespace, key, scope, key_type in PreferencesRegistry.all():
            if key_type == int:
                field = forms.IntegerField()
                field.type = "int"
            elif key_type == bool:
                field = forms.BooleanField()
                field.type = "bool"
            else:
                field = forms.CharField(max_length=255)
                field.type = "str"

            field.label = snake_case_to_spaces(key).capitalize()
            field.required = True

            # special attrs
            field.namespace = namespace
            field.scope = scope
            field.icon = ScopeIcons[scope.name.upper()]
            fieldname = "__".join(
                [
                    namespace,
                    key,
                    scope.name.lower(),
                ]
            )

            # "Enter a valid date/time."
            self.fields[fieldname] = field


class BooleanChoices(TextChoices):
    TRUE = _("True")
    FALSE = _("False")


class OnOffChoices(TextChoices):
    TRUE = _("On")
    FALSE = _("Off")


class ScopedPreferenceForm(forms.ModelForm):
    """This form dynamically creates a "value" field type for given scope.

    :param scope: the Scope the (new) setting should be in
    :param request: the current request. This is needed to determine
        the effective settings.
    :param base: The BasePreference the edited setting is based upon.
        Not needed if an instance is provided (e.g. in an UpdateView).
    """

    class Meta:
        model = ScopedPreference
        fields = ["value", "group", "tenant", "user"]  # TODO: , "device"

    def __init__(self, *args, **kwargs):
        self.scope = Scope[kwargs.pop("scope").upper()]
        if self.scope == Scope.VENDOR:
            raise PermissionError("Vendor settings cannot be edited.")

        self.request = kwargs.pop("request")
        # base is not available in create views
        self.base = kwargs.pop("base", None)
        super().__init__(*args, **kwargs)
        if not self.base:
            if not self.instance:
                raise AttributeError(
                    "ScopedPreferenceForm needs a BasePreference object. Either provide one directly "
                    "or call it with a ScopedPreference instance where it can read it."
                )
            self.base = self.instance.base
        key_type = self.base.key_type

        self.helper = FormHelper()
        self.helper.form_tag = False

        # change value field type according to key_type
        match key_type:
            case "str":
                value_field = forms.CharField(required=True)
                crispy_field = "value"
            case "text":
                value_field = forms.CharField(required=True, widget=forms.Textarea)
                crispy_field = "value"
            case "int":
                value_field = forms.IntegerField(
                    required=True,
                    # widget=forms.NumberInput()
                )
                crispy_field = "value"
            case "bool":
                value_field = forms.CharField(required=True)
                crispy_field = BooleanTextFieldButtonsGroup(
                    "value",
                )
            case _:
                raise TypeError(f"Wrong key_type at {self.base}")

        # when using this form in an UpdateView (existing instance
        # model available), convert boolean False to ""
        if self.instance.pk:
            if key_type == KeyType.BOOLEAN:
                value_field.initial = str_to_bool(self.instance.value)

        # when using the form in a CreateView, get the initial value from
        # the (until now) effective setting
        else:
            initial_value = get_effective_setting(
                self.base.namespace, self.base.key, self.request
            ).value
            # Convert Boolean string to template-usable one.
            if key_type == KeyType.BOOLEAN:
                initial_value = str_to_bool(initial_value)
            value_field.initial = initial_value

            # transfer monkey-patched .effective to field
            # TODO: is this needed?
            value_field.effective = False

        self.fields["value"] = value_field
        layout = [crispy_field]

        for scope in [Scope.TENANT, Scope.GROUP, Scope.USER]:  # TODO: add Scope.DEVICE
            scope_name = scope.name.lower()  # tenant, group, user, device
            # hide related objects if not matching scope
            if not scope == self.scope:
                # remove fields from HTML, don't just hide it.
                del self.fields[scope_name]
            else:
                self.fields[scope_name].disabled = True
                layout.append(Field(scope_name))

        match self.scope:
            case Scope.TENANT:
                self.fields["tenant"].initial = self.request.user.tenant
                if not self.request.user.is_superuser:
                    # only render one tenant. Users shouldn't see other tenants in the HTML.
                    # FIXME: use a PermissionAwareObjectManager for that, if possible
                    self.fields["tenant"].queryset = Tenant.objects.filter(
                        id=self.request.user.tenant.id
                    )
            case Scope.GROUP:
                # you can just set group settings for groups you are a member in
                self.fields["group"].queryset = self.request.user.groups.all()

        self.helper.layout = Layout(*layout)

    def save(self, commit=True):
        # set scope and base, only "value" (and, if available, group/device)
        # is editable in form.
        self.instance.base = self.base
        self.instance.scope = self.scope
        match self.scope:
            case Scope.TENANT:
                self.instance.tenant = self.request.user.tenant
            case Scope.GROUP:
                pass  # FIXME: implement group field if scope==GROUP
            case Scope.USER:
                self.instance.user = self.request.user
            case Scope.DEVICE:
                pass  # FIXME: implement device field if scope==DEVICE
        super().save(commit)
