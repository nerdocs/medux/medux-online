from django.db.models.signals import post_save
from django.dispatch import receiver


@receiver(post_save, sender="preferences.ScopedPreference")
def update_settings_item_component(sender, **kwargs):
    ...
