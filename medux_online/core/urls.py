from django.contrib.auth import views as auth_views
from django.urls import path, include

import medux.common.views
import medux_online.core.views as views
from medux.common.api.interfaces import IHTMXComponentMixin
from .api.interfaces import IDashboardURL

app_name = "core"

tenant_patterns = [
    path("", views.TenantListView.as_view(), name="list"),
    path("add/", views.TenantCreateView.as_view(), name="add"),
    path("<pk>/", views.TenantUpdateView.as_view(), name="detail"),
    path("<pk>/change/", views.TenantUpdateView.as_view(), name="update"),
    path("<pk>/delete/", views.TenantDeleteView.as_view(), name="delete"),
    path("<uuid:uuid>/privacy", views.PrivacyView.as_view(), name="privacy"),
    path("<uuid:uuid>/terms", views.TermsView.as_view(), name="terms"),
]

preferences_patterns = [
    path("", views.BasePreferenceListView.as_view(), name="overview"),
    # ScopedPreference CRUD
    path(
        "basepreference/<base_pk>/add-<str:scope>/",
        views.ScopedPreferenceCreateView.as_view(),
        name="add",
    ),
    path(
        "basepreference/<pk>/",
        views.BasePreferenceDetailView.as_view(),
        name="base-detail",
    ),
    path(
        "scopedpreference/<int:pk>/",
        # scopedpreference_detail,
        views.ScopedPreferenceDetailView.as_view(),
        name="detail",
    ),
    path(
        "scopedpreference/<int:pk>/update/",
        views.scopedpreference_update,
        name="update",
    ),
    path(
        "scopedpreference/<int:pk>/delete/",
        views.ScopedPreferenceDeleteView.as_view(),
        name="delete",
    ),
    path("set-tenant/<int:pk>/", views.set_active_tenant, name="set-tenant"),
]
# user_profile_patterns = [
#     # user:profile: namespace
#     path("masterdata", views.UserProfileMasterDataView.as_view(), name="master-data"),
#     path("password", views.UserProfilePasswordView.as_view(), name="password"),
#     path("groups", views.UserProfileGroupsView.as_view(), name="groups"),
# ]
user_patterns = [
    # user: namespace
    path("", views.UserListView.as_view(), name="list"),
    path(
        "<int:pk>/detail/", medux.common.views.UserUpdateView.as_view(), name="detail"
    ),
    path("add/", views.UserCreateView.as_view(), name="add"),
    path(
        "<int:pk>/update/", medux.common.views.UserUpdateView.as_view(), name="update"
    ),
    path("<int:pk>/delete/", views.UserDeleteView.as_view(), name="delete"),
    path("profile/", medux.common.views.UserProfileView.as_view(), name="profile"),
]

root_urlpatterns = [
    path("auth/", auth_views.PasswordChangeView.as_view()),
    # FIXME: should be included in Conjunto...
    # path(
    #     "__elements__/",
    #     include(
    #         (IHtmxComponentMixin.get_url_patterns(), "components"),
    #         namespace="components",
    #     ),
    # ),
]


class CoreDashboardURLs(IDashboardURL):
    urlpatterns = [
        path("tenant/", include((tenant_patterns, "tenant"), namespace="tenant")),
        path(
            "preferences/",
            include((preferences_patterns, "preferences"), namespace="preferences"),
        ),
        path(
            "user/",
            include((user_patterns, "user"), namespace="user"),
        ),
    ]
