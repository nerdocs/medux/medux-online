from django.test import TestCase
from gdaps.management.commands.initializeplugins import (
    Command as InitializePluginsCommand,
)

from medux.common.management.commands.initialize import Command as InitializeCommand
from medux.common.management.commands.loadpreferences import (
    Command as LoadPreferencesCommand,
)
from medux.common.models import Tenant
from medux_online.core.models import User


class MeduxOnlineTestCase(TestCase):
    """A Basic Test case every test in MeduxOnline should inherit from.

    TODO: refactor tests to use self.client.get() again

    For convenience, there are ``login()`` and ``logout()`` methods available.

    Class attributes ``user1``, ``user2``, and ``admin`` are available
    and link to created users.
    """

    admin: User = None
    admin_user: User = None
    user1: User = None
    user2: User = None

    admin_site = None

    admin_tenant: Tenant = None
    tenant1: Tenant = None
    tenant2: Tenant = None

    @classmethod
    def setUpTestData(cls):
        """Set up data for the whole TestCase, which should not be changed.

        * calls mgmt commands ``initialize``, ``initializeplugins``,
          ``loadsettings``
        * cls.tenant is the "Admin" tenant
        * creates admin, user1 and user2 users
        """

        InitializeCommand().handle()
        InitializePluginsCommand().handle()
        LoadPreferencesCommand().handle()

        cls.admin_tenant = Tenant.objects.get(pk=1)  # Admin

        cls.tenant1 = Tenant.objects.create(
            title="Dr.", first_name="Theophrastus", last_name="von Hohenheim"
        )
        cls.tenant2 = Tenant.objects.create(
            title="Dr.", first_name="Leonard", last_name="McCoy"
        )
        cls.admin = User.objects.get(username="admin")
        cls.admin.tenant = cls.admin_tenant
        cls.admin.save()

        cls.user1 = User.objects.create_user(username="user1", password="user1")
        cls.user1.tenant = cls.tenant1
        cls.user1.save()

        cls.user2 = User.objects.create_user(username="user2", password="user2")
        cls.user2.tenant = cls.tenant2
        cls.user2.save()

    def get(self, *args, **kwargs):
        """deprecated"""
        raise DeprecationWarning("Please use self.client.get() instead")

    def post(self, *args, **kwargs):
        """deprecated"""
        raise DeprecationWarning("Please use self.client.post() instead")

    def delete(self, *args, **kwargs):
        """calls DELETE on the client, with the current server name"""
        raise DeprecationWarning("Please use self.client.delete() instead")

    def login(self, user: str, password: str):
        """logs the given user in."""
        return self.client.login(username=user, password=password)

    def logout(self):
        """Log out from the current host."""
        return self.client.post("/accounts/logout/")
