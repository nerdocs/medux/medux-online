from django.conf import settings
from django.contrib.sites.models import Site
from django.contrib.sites.shortcuts import get_current_site

from medux_online.core.tests import MeduxOnlineTestCase


class SettingsTest(MeduxOnlineTestCase):
    def test_SITE_ID_1(self):
        """test if get_current_site returns correct site while testing"""
        fake_request = None
        self.assertEqual(1, get_current_site(fake_request).id)

    def test_override_SITE_ID(self):
        """make sure that overriding SITE_ID works for tests"""
        site = Site.objects.create(domain="example234.com", name="Example 234")
        settings.SITE_ID = site.id
        fake_request = None
        self.assertEqual(site.id, get_current_site(fake_request).id)
