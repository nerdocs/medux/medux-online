from django.contrib.auth.models import Group, Permission
from django.urls import reverse

from medux.preferences.definitions import Scope
from medux.preferences.models import ScopedPreference, BasePreference
from medux.preferences.registry import PreferencesRegistry
from medux_online.core.tests import MeduxOnlineTestCase


class AccessTest(MeduxOnlineTestCase):
    """Tests access for various core URLs"""

    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()

        site_editors = Group.objects.get(name="Site editors")
        cls.user1.groups.add(site_editors)
        PreferencesRegistry.register(
            namespace="foo", key="bar", allowed_scopes=[Scope.TENANT]
        )

    def test_dashbord_access_denied_for_anonymous(self) -> None:
        response = self.client.get(reverse("index"))
        self.assertRedirects(response, "/accounts/login/?next=/dashboard/")

    def test_dashboard_access_with_login(self) -> None:
        self.client.force_login(self.user1)

        response = self.client.get(reverse("index"))
        self.assertEqual(200, response.status_code)

    # def test_dashboard_access_with_wrong_user_login(self) -> None:
    #     self.assertTrue(self.client.force_login(self.user2))
    #
    #     response = self.client.get(reverse("index"))
    #     self.assertEqual(403, response.status_code)

    # TODO: tenant/
    # TODO: tenant/<pk>/
    # TODO: tenant/<pk>/edit/

    def test_settings_access(self):
        response = self.client.get("/dashboard/preferences/")
        self.assertRedirects(response, "/accounts/login/?next=/dashboard/preferences/")
        self.client.force_login(self.user1)
        response = self.client.get("/dashboard/preferences/")
        self.assertEqual(response.status_code, 200)

    def test_settings_basesetting_no_access(self):
        response = self.client.get(reverse("preferences:base-detail", kwargs={"pk": 1}))
        self.assertRedirects(
            response, "/accounts/login/?next=/dashboard/preferences/basepreference/1/"
        )

    def test_settings_basesetting_with_access(self):

        self.client.force_login(self.user1)
        response = self.client.get(reverse("preferences:base-detail", kwargs={"pk": 1}))
        self.assertEqual(200, response.status_code)

    def test_settings_scopedpreference_access(self):
        self.client.force_login(self.user1)
        self.user1.user_permissions.add(
            Permission.objects.get(codename="view_scopedpreference")
        )
        self.user1 = User.objects.get(pk=self.user1.pk)
        response = self.client.get(reverse("preferences:detail", kwargs={"pk": 1}))
        self.assertEqual(200, response.status_code)

    def test_delete_vendor_setting(self):
        self.client.force_login(self.user1)
        vendor_setting = ScopedPreference.objects.filter(scope=Scope.VENDOR).first()
        response = self.client.post(
            reverse("preferences:delete", kwargs={"pk": vendor_setting.pk}),
            follow=True,
        )
        # should respond with "Forbidden"
        self.assertEqual(403, response.status_code)

    def test_delete_tenant_preference_without_permission(self):
        self.client.force_login(self.user1)

        base_setting = BasePreference.objects.create(namespace="foo", key="bar")
        tenant_setting = ScopedPreference.objects.create(
            base=base_setting, scope=Scope.TENANT, tenant=self.admin_tenant, value="foo"
        )
        response = self.client.post(
            reverse("preferences:delete", kwargs={"pk": tenant_setting.pk}),
            follow=True,
        )
        # should respond with "Forbidden"
        self.assertEqual(response.status_code, 403)

    def test_delete_tenant_setting_with_permission(self):
        self.user1.user_permissions.add(
            Permission.objects.get(
                codename="change_own_tenant_scopedpreference",
            )
        )
        # refetch user from the database
        self.user1 = User.objects.get(pk=self.user1.pk)
        self.client.force_login(self.user1)

        base_setting = BasePreference.objects.create(namespace="foo", key="bar")
        tenant_preference = ScopedPreference.objects.create(
            base=base_setting, scope=Scope.TENANT, tenant=self.tenant1, value="foo"
        )
        response = self.client.post(
            reverse(
                "preferences:delete",
                kwargs={"pk": tenant_preference.pk},
            ),
            follow=True,
        )
        self.assertEqual(200, response.status_code)
