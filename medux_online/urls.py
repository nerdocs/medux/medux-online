from functools import wraps

from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.core.exceptions import PermissionDenied
from django.urls import path, include
from gdaps.pluginmanager import PluginManager

from .core.api.interfaces import IDashboardURL
from .core.views import DashboardView

app_name = "medux_online"


def medux_site_required(test_func=None):
    """
    Decorator for views that checks that the current site is the main
    medux site. If not, raise a PermissionDenied error
    """

    def site_is_medux(view_func):
        @wraps(view_func)
        def _wrapped_view(request, *args, **kwargs):
            if request.site == 1:
                return True
            raise PermissionDenied

        return _wrapped_view

    return site_is_medux


# create a pluggable "dashboard" urlpatterns hook
dashboard_urlpatterns = [
    path("", DashboardView.as_view(), name="index"),
]
for up in IDashboardURL:
    dashboard_urlpatterns += up.urlpatterns

urlpatterns = [
    # TODO make admin only available on SIDE_ID=1
    path("admin/", admin.site.urls, name="admin"),
    path(
        "dashboard/",
        # dashboard is no namespace, only part of the URL
        include((dashboard_urlpatterns, ""), namespace=""),
        name="index",
    ),
    path("__debug__/", include("debug_toolbar.urls")),
] + PluginManager.urlpatterns()

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
