# Changelog of MedUX online
This Project uses Semantic versioning 2.0, see https://semver.org

## [0.0.5] - unreleased
### Added
- staff users can change their tenants dynamically
- support for push notifications as toasts
- moved from pytest to Django Test framework

## [0.0.4] - 2022-05-15
### Changed
- Unicorn -> Turbo-Django

## [0.0.3] - 2022-05-15
### Changed
- properly rename "Client" to "Tenant"

## [0.0.2] - 2022-05-01
### Added
- Dashboard with pluggable views
- multiple, configurable hosts

## [0.0.1]
- initial version
- separated homepages and prescriptions sites