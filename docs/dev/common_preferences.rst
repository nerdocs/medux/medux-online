.. note::

    TODO: This should be cross-referenced to medux.common

.. autoclass:: medux.preferences.models.ScopedPreferences
    :members:
.. autoclass:: medux.common.models.Tenant
    :members:
.. autoclass:: medux.common.models.TenantModelMixin
    :members: