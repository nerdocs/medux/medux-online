===========
Development
===========


.. toctree::
    :maxdepth: 3

    dashboard


Code style
----------

No compromises. Format your code using `Black <https://black.readthedocs.io/en/stable/>`_ before committing.