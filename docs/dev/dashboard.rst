=============
The Dashboard
=============

The Dashboard is a central place for administrating Medux-Online.
It can be extended by plugins very easily.

Per default, the dashboard is only available under the primary site.