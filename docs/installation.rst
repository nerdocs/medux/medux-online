============
Installation
============

While not feature complete, just install it like any other python software using pip, pipenv, or poetry.

.. code-block:: bash

    # create and activate a Virtualenv
    python -m virtualenv .venv
    . .venv/bin/activate

    install medux-online
    pip install medux-online


Now initialize the application with

.. code-block:: bash

    ./manage.py makemigrations  # still needed during alpha
    ./manage.py migrate
    ./manage.py initialize

.. note::
    During early development, the ``makemigrations`` command is still needed, as models change too often to provide a clean upgrade path using migrations.

The ``initialize`` management command loads fixtures and settings needed for the first run, and creates an **"admin" user** with the **password "admin"**. Change that password for production use!


Redis
=====

You will need a Redis_ server for MedUX-Online to work. Have a look how to install it on their homepage. There are instructions for Linux_ and Windows_.
For Ubuntu/Debian, a simple

.. code-block:: bash

    apt install redis

will do and install the Distribution maintainer's version, which is ok.


.. _Redis: https://redis.com
.. _Linux: https://redis.io/docs/getting-started/installation/install-redis-on-linux/
.. _Windows: https://redis.io/docs/getting-started/installation/install-redis-on-windows/


Development server
==================

Start a Django development server with

.. code-block:: bash

    ./manage.py runserver

...and test it out.