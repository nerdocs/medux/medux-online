# Plugin: Prescriptions

This is a plugin that provides functionality for public accessible medication prescription requests.

## Models
::: medux_online.plugins.prescriptions.models

## Views
::: medux_online.plugins.prescriptions.views

::: medux_online.plugins.prescriptions.streams

