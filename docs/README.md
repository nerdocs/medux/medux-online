# MedUX Online Documentation

Welcome to the MedUX Online documentation!

MedUX Online is a complete system for online presence of medical professionals. It is pluggable, and seeks for feature completeness in *Near Future™*...

!!! warning

    This software is in an early development state (WIP). It's not considered to be used in production yet.
    Use it at your own risk. **You have been warned.**


    [installation](installation.md)
    [APIs](APIs.md)
    [contributing](contributing.md)
    [development](development.md)


## License

I'd like to give back what I received from many Open Source software packages, and keep this project as open as possible, and it should stay this way.
MedUX online is licensed under the `GNU Affero General Public License, version 3](https://www.gnu.org/licenses/agpl-3.0.en.html).
